//
//  CuencaViewController.swift
//  climapp
//
//  Created by Gabriela Cuascota on 29/5/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import UIKit

class CuencaViewController: UIViewController {

    @IBOutlet weak var ingresarCiudad: UITextField!
    
    @IBOutlet weak var resultadoConsulta: UILabel!
    
    @IBAction func concultarClima(_ sender: Any) {
        
        
        
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(ingresarCiudad.text ?? "quito")&appid=391e2173613a3517cc476e80418ba22c"
    
        var url = URL(string: urlString)
        
        var session = URLSession.shared
        
        var task = session.dataTask(with: url!) { (data,response, error) in
            
            guard let data = data else {
                print ("Error NO data")
                return
            }
            guard let weatherInfo = try? JSONDecoder().decode(CuencaWeatherInfo.self, from: data) else {
                print("Error decoding Weather")
                return
            }
            DispatchQueue.main.async {
                self.resultadoConsulta.text = "\(weatherInfo.weather[0].description)"
            }

        }
        task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
