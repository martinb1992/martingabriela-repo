//
//  cuencaWeatherController.swift
//  climapp
//
//  Created by Gabriela Cuascota on 30/5/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import Foundation


struct CuencaWeatherInfo: Decodable {
    let weather: [CuencaWeather]
}

struct CuencaWeather: Decodable {
    let id: Int
    let description: String
    
}


