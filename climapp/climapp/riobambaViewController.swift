//
//  riobambaViewController.swift
//  climapp
//
//  Created by Martin Balarezo on 29/5/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import UIKit

class riobambaViewController: UIViewController {

    
    @IBOutlet weak var resultadoClimaLBL: UILabel!
    
    @IBAction func consultarClimaBTN(_ sender: Any) {
        
        
        var urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(ingresarCiudadTXT.text ?? "quito")&APPID=7834acc4c4534177fc46874fef66c8f4"
        
        var url = URL(string: urlString)
        
        
        var session = URLSession.shared
        
        var task = session.dataTask(with: url!) { (data, response, error) in
            print(data)
            guard let data = data else{
                print("Error NO data")
                return
            }
            guard let weatherInfo = try? JSONDecoder().decode(RiobambaWeatherInfo.self, from: data) else {
                print("Error Decoding Weather")
                return
                
            }
            
            print(weatherInfo.weather[0].description)
            
            DispatchQueue.main.async {
                self.resultadoClimaLBL.text = "\(weatherInfo.weather[0].description)"
            }
            
            
            
        }
        
        task.resume()
        
        
        
        
        
    }
    
    @IBOutlet weak var ingresarCiudadTXT: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
