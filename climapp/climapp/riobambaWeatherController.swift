//
//  riobambaWeatherController.swift
//  climapp
//
//  Created by Martin Balarezo on 30/5/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import Foundation

struct RiobambaWeatherInfo: Decodable {
    let weather: [RiobambaWeather]
    
    
}

struct RiobambaWeather: Decodable {
    let id: Int
    let description: String
    
}
